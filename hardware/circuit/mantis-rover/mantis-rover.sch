EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Conexión puente H a partir de reles"
Date ""
Rev ""
Comp "Johnny Cubides"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Relay:G6E K1
U 1 1 5DD00C0A
P 6700 2400
F 0 "K1" H 7130 2446 50  0000 L CNN
F 1 "G6E" H 7130 2355 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G6E" H 7830 2370 50  0001 C CNN
F 3 "https://www.omron.com/ecb/products/pdf/en-g6e.pdf" H 6700 2400 50  0001 C CNN
	1    6700 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5DD0126B
P 6500 1600
F 0 "#PWR?" H 6500 1450 50  0001 C CNN
F 1 "+12V" H 6515 1773 50  0000 C CNN
F 2 "" H 6500 1600 50  0001 C CNN
F 3 "" H 6500 1600 50  0001 C CNN
	1    6500 1600
	1    0    0    -1  
$EndComp
$Comp
L pspice:DIODE D1
U 1 1 5DD01B95
P 5550 2400
F 0 "D1" V 5596 2272 50  0000 R CNN
F 1 "1N4004" V 5505 2272 50  0000 R CNN
F 2 "" H 5550 2400 50  0001 C CNN
F 3 "~" H 5550 2400 50  0001 C CNN
	1    5550 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2200 5550 1850
Wire Wire Line
	5550 1850 6500 1850
Wire Wire Line
	6500 1850 6500 2100
Wire Wire Line
	6500 1600 6500 1850
Connection ~ 6500 1850
Wire Wire Line
	5550 2600 5550 3100
Wire Wire Line
	5550 3100 6500 3100
Wire Wire Line
	6500 3100 6500 2700
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5DD03D89
P 4100 1650
F 0 "Q1" H 4291 1696 50  0000 L CNN
F 1 "2N2222A" H 4291 1605 50  0000 L CNN
F 2 "" H 4300 1750 50  0001 C CNN
F 3 "~" H 4100 1650 50  0001 C CNN
	1    4100 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DD04AC1
P 4200 2250
F 0 "#PWR?" H 4200 2000 50  0001 C CNN
F 1 "GND" H 4205 2077 50  0000 C CNN
F 2 "" H 4200 2250 50  0001 C CNN
F 3 "" H 4200 2250 50  0001 C CNN
	1    4200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1850 4200 2250
Wire Wire Line
	3050 1650 3450 1650
Text GLabel 3050 1650 0    50   Input ~ 0
inputA
Wire Wire Line
	3900 1650 3750 1650
$Comp
L Device:R R1
U 1 1 5DD05254
P 3600 1650
F 0 "R1" V 3393 1650 50  0000 C CNN
F 1 "1k" V 3484 1650 50  0000 C CNN
F 2 "" V 3530 1650 50  0001 C CNN
F 3 "~" H 3600 1650 50  0001 C CNN
	1    3600 1650
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_EBC Q2
U 1 1 5DD0D9E9
P 4100 2950
F 0 "Q2" H 4291 2996 50  0000 L CNN
F 1 "2N2222A" H 4291 2905 50  0000 L CNN
F 2 "" H 4300 3050 50  0001 C CNN
F 3 "~" H 4100 2950 50  0001 C CNN
	1    4100 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DD0D9EF
P 4200 3550
F 0 "#PWR?" H 4200 3300 50  0001 C CNN
F 1 "GND" H 4205 3377 50  0000 C CNN
F 2 "" H 4200 3550 50  0001 C CNN
F 3 "" H 4200 3550 50  0001 C CNN
	1    4200 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3150 4200 3550
Wire Wire Line
	3050 2950 3450 2950
Text GLabel 3050 2950 0    50   Input ~ 0
inputB
Wire Wire Line
	3900 2950 3750 2950
$Comp
L Device:R R2
U 1 1 5DD0D9F9
P 3600 2950
F 0 "R2" V 3393 2950 50  0000 C CNN
F 1 "1k" V 3484 2950 50  0000 C CNN
F 2 "" V 3530 2950 50  0001 C CNN
F 3 "~" H 3600 2950 50  0001 C CNN
	1    3600 2950
	0    1    1    0   
$EndComp
$Comp
L Relay:G6E K2
U 1 1 5DD16EDD
P 8350 2400
F 0 "K2" H 7920 2446 50  0000 R CNN
F 1 "G6E" H 7920 2355 50  0000 R CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G6E" H 9480 2370 50  0001 C CNN
F 3 "https://www.omron.com/ecb/products/pdf/en-g6e.pdf" H 8350 2400 50  0001 C CNN
	1    8350 2400
	-1   0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5DD16EE3
P 8550 1600
F 0 "#PWR?" H 8550 1450 50  0001 C CNN
F 1 "+12V" H 8565 1773 50  0000 C CNN
F 2 "" H 8550 1600 50  0001 C CNN
F 3 "" H 8550 1600 50  0001 C CNN
	1    8550 1600
	-1   0    0    -1  
$EndComp
$Comp
L pspice:DIODE D2
U 1 1 5DD16EE9
P 9500 2400
F 0 "D2" V 9546 2528 50  0000 L CNN
F 1 "1N4004" V 9455 2528 50  0000 L CNN
F 2 "" H 9500 2400 50  0001 C CNN
F 3 "~" H 9500 2400 50  0001 C CNN
	1    9500 2400
	0    1    -1   0   
$EndComp
Wire Wire Line
	9500 2200 9500 1850
Wire Wire Line
	9500 1850 8550 1850
Wire Wire Line
	8550 1850 8550 2100
Wire Wire Line
	8550 1600 8550 1850
Connection ~ 8550 1850
Wire Wire Line
	9500 2600 9500 3100
Wire Wire Line
	9500 3100 8550 3100
Wire Wire Line
	8550 3100 8550 2700
$Comp
L Motor:Motor_DC M1
U 1 1 5DD20ED8
P 7500 3150
F 0 "M1" V 7795 3100 50  0000 C CNN
F 1 "Motor_DC" V 7704 3100 50  0000 C CNN
F 2 "" H 7500 3060 50  0001 C CNN
F 3 "~" H 7500 3060 50  0001 C CNN
	1    7500 3150
	0    -1   -1   0   
$EndComp
Text GLabel 4350 1300 2    50   Input ~ 0
colectorA
Wire Wire Line
	4350 1300 4200 1300
Wire Wire Line
	4200 1300 4200 1450
Text GLabel 4450 2550 2    50   Input ~ 0
colectorB
Wire Wire Line
	4200 2750 4200 2550
Wire Wire Line
	4200 2550 4450 2550
Text GLabel 6350 3350 0    50   Input ~ 0
colectorA
Text GLabel 8700 3450 2    50   Input ~ 0
colectorB
Wire Wire Line
	6350 3350 6500 3350
Wire Wire Line
	6500 3350 6500 3100
Connection ~ 6500 3100
Wire Wire Line
	8700 3450 8550 3450
Wire Wire Line
	8550 3450 8550 3100
Connection ~ 8550 3100
$Comp
L power:+12V #PWR?
U 1 1 5DD29E07
P 7000 1850
F 0 "#PWR?" H 7000 1700 50  0001 C CNN
F 1 "+12V" H 7015 2023 50  0000 C CNN
F 2 "" H 7000 1850 50  0001 C CNN
F 3 "" H 7000 1850 50  0001 C CNN
	1    7000 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 5DD2A3E5
P 8050 1900
F 0 "#PWR?" H 8050 1750 50  0001 C CNN
F 1 "+12V" H 8065 2073 50  0000 C CNN
F 2 "" H 8050 1900 50  0001 C CNN
F 3 "" H 8050 1900 50  0001 C CNN
	1    8050 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DD2AC36
P 6800 1850
F 0 "#PWR?" H 6800 1600 50  0001 C CNN
F 1 "GND" H 6805 1677 50  0000 C CNN
F 2 "" H 6800 1850 50  0001 C CNN
F 3 "" H 6800 1850 50  0001 C CNN
	1    6800 1850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DD2BAFA
P 8250 1900
F 0 "#PWR?" H 8250 1650 50  0001 C CNN
F 1 "GND" H 8255 1727 50  0000 C CNN
F 2 "" H 8250 1900 50  0001 C CNN
F 3 "" H 8250 1900 50  0001 C CNN
	1    8250 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 2700 6900 3150
Wire Wire Line
	6900 3150 7300 3150
Wire Wire Line
	7800 3150 7900 3150
Wire Wire Line
	8150 3150 8150 2700
Wire Wire Line
	6800 1850 6800 2100
Wire Wire Line
	7000 1850 7000 2100
Wire Wire Line
	8050 1900 8050 2100
Wire Wire Line
	8250 1900 8250 2100
Connection ~ 7900 3150
Wire Wire Line
	7900 3150 8150 3150
Text Notes 6150 4050 0    50   ~ 0
inputA inputB | ReleA ReleB   | Motor \n    0    0    |  GND    GND  | Detenido\n    0    1    |  GND     12V | Giro\n    1    0    |  12V     GND | Contragiro\n    1    1    |  12V    12V  | Detenido\n
Text Notes 6050 4550 0    50   ~ 0
Observación: fijarse que en los contactos del relé NC están conectados\nlos niveles de tensión GND, ésto con el fin de impedir que el vehículo\nque el motor quede energizado sin la acción del controlador\napartir de los input
$EndSCHEMATC
