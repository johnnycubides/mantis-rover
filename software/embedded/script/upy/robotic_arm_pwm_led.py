import socket as sc
import network as nw
import time as tm
from machine import Pin
from machine import PWM


class Motor:

    def __init__(self, pina, pinb):
        self.pina = PWM(Pin(pina, Pin.OUT), freq=1000, duty=0)
        self.pinb = PWM(Pin(pinb, Pin.OUT), freq=1000, duty=0)

    def turn(self, dutya, dutyb):
        self.pina.duty(dutya)
        self.pinb.duty(dutyb)

    def action(self, word):
        # print(word)
        if word >= 97 and word <= 122:
            dutya = 0
            dutyb = int((word - 97)*40.92)
        elif word >= 65 and word <= 90:
            dutya = int((90 - word)*40.92)
            dutyb = 0
        else:
            dutya = 0
            dutyb = 0
        # print("da "+str(dutya))
        # print("db "+str(dutyb))
        self.turn(dutya, dutyb)


class Pincer:

    def __init__(self, pina, pinb):
        self.pina = Pin(pina, Pin.OUT)
        self.pinb = Pin(pinb, Pin.OUT)

    def action(self, word):
        if word == '0':
            self.pina.value(0)
            self.pinb.value(0)
        elif word == '1':
            self.pina.value(1)
            self.pinb.value(0)
            tm.sleep_ms(50)
            self.action('0')
        elif word == '2':
            self.pina.value(0)
            self.pinb.value(1)
            tm.sleep_ms(50)
            self.action('0')
        else:
            self.action('0')


devices = [Motor(13, 12), Motor(14, 27),
           Motor(26, 25), Motor(33, 32),
           Pincer(2, 4),
           Pin(15, Pin.OUT)]

# create station interface
wlan = nw.WLAN(nw.STA_IF)
# activate the interface
wlan.active(True)
# scan for access points
# wlan.scan()
# check if the station is connected to an AP
wlan.isconnected()
# connect to an AP
# wlan.connect('mantis', '13578642')
wlan.connect('UNE_HFC_EA1F', 'XQIDC9HE')
# get the interface's MAC address
wlan.config('mac')
# get the interface's IP/netmask/gw/DNS addresses
wlan.ifconfig()

# # create access-point interface
# ap = nw.WLAN(nw.AP_IF)
# # set the ESSID of the access point
# ap.config(essid='ESP-AP')
# # activate the interface
# ap.active(True)


def do_connect():
    # wlan = nw.WLAN(nw.STA_IF)
    # wlan.active(True)
    if not wlan.isconnected():
        print('connecting to nw...')
        # wlan.connect('essid', 'password')
        while not wlan.isconnected():
            pass
    print('nw config:', wlan.ifconfig())


do_connect()


html = """<!DOCTYPE html>
<html>
    <head> <title>ESP8266 Pins</title> </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

addr = sc.getaddrinfo('0.0.0.0', 80)[0][-1]

s = sc.socket()
s.bind(addr)
s.listen(1)


def decode(data):
    if len(data) == 7:
        # devices[2].action(chr(data[0]))
        # print(data)
        # print(data[0])
        # devices[0].action(data[0])
        for i in [0, 1, 2, 3]:
            devices[i].action(data[i])
        devices[4].action(chr(data[4]))
        devices[5].value(1) if chr(data[5]) == '1' else devices[5].value(0)
    else:
        for i in [0, 1, 2, 3]:
            devices[i].action(0)
        devices[4].action('0')
        devices[5].value(0)


while True:
    cl, addr = s.accept()
    print('client connected from', addr)
    cl_file = cl.makefile('rwb', 0)
    while True:
        # line = cl_file.read(7)
        line = cl_file.readline()
        # print(line)
        decode(line)
        if line == b'':
            break
        # if not line or line == b'\r\n':
        #     print(line)
        #     print("this")
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    response = html % '\n'.join(rows)
    cl.send(response)
    cl.close()
