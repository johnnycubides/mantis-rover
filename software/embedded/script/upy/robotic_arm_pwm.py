import socket as sc
import network as nw
from machine import Pin
from machine import PWM
import time as tmr


class Motor:
    dutyF = 256 + 32 + 8
    dutyB = 1023 - 256 - 128

    def __init__(self, pina, pinb):
        self.pina = Pin(pina, Pin.OUT)
        self.pinb = PWM(Pin(pinb, Pin.OUT), freq=1000, duty=self.dutyF)
        self.turn(0)

    def turn(self, instruction):
        if instruction == 0:
            self.pina.value(0)
            self.pinb.duty(0)
        elif instruction == 1:
            self.pina.value(0)
            self.pinb.duty(1023)
            tmr.sleep_ms(50)
            self.pinb.duty(self.dutyF)
        elif instruction == 2:
            self.pina.value(1)
            self.pinb.duty(0)
            tmr.sleep_ms(50)
            self.pinb.duty(self.dutyB)
        else:
            self.pina.value(0)
            self.pinb.duty(0)

    def action(self, word):
        print(word)
        if word == 'A':
            self.turn(1)
        elif word == 'B':
            self.turn(2)
        elif word == 'C':
            self.turn(0)
        else:
            self.turn(0)


motors = [Motor(15, 13), Motor(2, 12),
          Motor(4, 14), Motor(16, 27),
          Motor(17, 25)]

# create station interface
wlan = nw.WLAN(nw.STA_IF)
# activate the interface
wlan.active(True)
# scan for access points
# wlan.scan()
# check if the station is connected to an AP
wlan.isconnected()
# connect to an AP
# wlan.connect('mantis', '13578642')
wlan.connect('UNE_HFC_EA1F', 'XQIDC9HE')
# get the interface's MAC address
wlan.config('mac')
# get the interface's IP/netmask/gw/DNS addresses
wlan.ifconfig()

# # create access-point interface
# ap = nw.WLAN(nw.AP_IF)
# # set the ESSID of the access point
# ap.config(essid='ESP-AP')
# # activate the interface
# ap.active(True)


def do_connect():
    # wlan = nw.WLAN(nw.STA_IF)
    # wlan.active(True)
    if not wlan.isconnected():
        print('connecting to nw...')
        # wlan.connect('essid', 'password')
        while not wlan.isconnected():
            pass
    print('nw config:', wlan.ifconfig())


do_connect()


html = """<!DOCTYPE html>
<html>
    <head> <title>ESP8266 Pins</title> </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

addr = sc.getaddrinfo('0.0.0.0', 80)[0][-1]

s = sc.socket()
s.bind(addr)
s.listen(1)


def decode(data):
    if len(data) == 5:
        motors[2].action(chr(data[0]))
        # for i in [0, 1, 2, 3, 4]:
            # motors[i].action(chr(data[i]))
    else:
        for i in [0, 1, 2, 3, 4]:
            motors[i].action('C')


while True:
    cl, addr = s.accept()
    print('client connected from', addr)
    cl_file = cl.makefile('rwb', 0)
    while True:
        line = cl_file.read(5)
        # line = cl_file.readline()
        print(line)
        decode(line)
        if line == b'':
            break
        # if not line or line == b'\r\n':
        #     print(line)
        #     print("this")
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    response = html % '\n'.join(rows)
    cl.send(response)
    cl.close()
