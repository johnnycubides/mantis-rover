import socket as sc
import network as nw
from machine import Pin


pins = [Pin(i, Pin.OUT) for i in (5, 4, 0, 2)]
values = [0, 0, 0, 0]
for i in [0, 1, 2, 3]:
    pins[i].value(values[i])


# create station interface
wlan = nw.WLAN(nw.STA_IF)
# activate the interface
wlan.active(True)
# scan for access points
# wlan.scan()
# check if the station is connected to an AP
wlan.isconnected()
# connect to an AP
wlan.connect('mantis', '13578642')
# get the interface's MAC address
wlan.config('mac')
# get the interface's IP/netmask/gw/DNS addresses
wlan.ifconfig()

# # create access-point interface
# ap = nw.WLAN(nw.AP_IF)
# # set the ESSID of the access point
# ap.config(essid='ESP-AP')
# # activate the interface
# ap.active(True)


def do_connect():
    # wlan = nw.WLAN(nw.STA_IF)
    # wlan.active(True)
    if not wlan.isconnected():
        print('connecting to nw...')
        # wlan.connect('essid', 'password')
        while not wlan.isconnected():
            pass
    print('nw config:', wlan.ifconfig())


do_connect()


html = """<!DOCTYPE html>
<html>
    <head> <title>ESP8266 Pins</title> </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

addr = sc.getaddrinfo('0.0.0.0', 80)[0][-1]

s = sc.socket()
s.bind(addr)
s.listen(1)


def decode(data):
    values = [0, 0, 0, 0]
    # print(data)
    if data == b'A':
        values = [0, 1, 0, 1]
    elif data == b'B':
        values = [1, 0, 1, 0]
    elif data == b'C':
        values = [1, 0, 0, 1]
    elif data == b'D':
        values = [0, 1, 1, 0]
    elif data == b'E':
        values = [0, 0, 0, 0]
    else:
        values = [0, 0, 0, 0]
    for i in [0, 1, 2, 3]:
        # print(i)
        pins[i].value(values[i])


while True:
    cl, addr = s.accept()
    print('client connected from', addr)
    cl_file = cl.makefile('rwb', 0)
    while True:
        line = cl_file.read(1)
        # line = cl_file.readline()
        print(line)
        decode(line)
        if line == b'':
            break
        # if not line or line == b'\r\n':
        #     print(line)
        #     print("this")
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    response = html % '\n'.join(rows)
    cl.send(response)
    cl.close()
