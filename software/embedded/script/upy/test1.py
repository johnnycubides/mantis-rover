import socket as sc
import network as nw
from machine import Pin

# create station interface
wlan = nw.WLAN(nw.STA_IF)
# activate the interface
wlan.active(True)
# scan for access points
# wlan.scan()
# check if the station is connected to an AP
wlan.isconnected()
# connect to an AP
wlan.connect('mantis', '13578642')
# get the interface's MAC address
wlan.config('mac')
# get the interface's IP/netmask/gw/DNS addresses
wlan.ifconfig()

# # create access-point interface
# ap = nw.WLAN(nw.AP_IF)
# # set the ESSID of the access point
# ap.config(essid='ESP-AP')
# # activate the interface
# ap.active(True)


def do_connect():
    # wlan = nw.WLAN(nw.STA_IF)
    # wlan.active(True)
    if not wlan.isconnected():
        print('connecting to nw...')
        # wlan.connect('essid', 'password')
        while not wlan.isconnected():
            pass
    print('nw config:', wlan.ifconfig())


do_connect()

pins = [Pin(i, Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]

html = """<!DOCTYPE html>
<html>
    <head> <title>ESP8266 Pins</title> </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

addr = sc.getaddrinfo('0.0.0.0', 80)[0][-1]

s = sc.socket()
s.bind(addr)
s.listen(1)

while True:
    cl, addr = s.accept()
    print('client connected from', addr)
    cl_file = cl.makefile('rwb', 0)
    while True:
        line = cl_file.readline()
        print('help ' % str(line))
        if not line or line == b'\r\n':
            break
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    response = html % '\n'.join(rows)
    cl.send(response)
    cl.close()
